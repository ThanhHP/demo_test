package com.example.demo;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.demo.controllers.AbstractController;
import com.example.demo.entities.Work;
import com.example.demo.enums.StatusEnum;
import com.example.demo.models.WorkEditRequest;
import com.example.demo.models.WorkRegisterRequest;
import com.example.demo.models.WorkSearchRequest;
import com.example.demo.repositories.WorkRepository;
import com.example.demo.services.WorkService;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class WorkServiceTest extends AbstractController {

    @MockBean
    WorkRepository workrepository;
    @Autowired
    private WorkService workService;

    @Test
    public void testAddWorks() throws Exception {

        WorkRegisterRequest workRegisterRequest = new WorkRegisterRequest();
        workRegisterRequest.setWorkName("test");
        Work workrs = new Work();
        workrs.setWorkName("test");
        Mockito.when(workrepository.save(Mockito.any(Work.class))).thenReturn(workrs);

        Work w = workService.save(workRegisterRequest);
        Assert.assertTrue("test".equals(w.getWorkName()));
    }

    @Test
    public void testFindListWorks() throws Exception {
        Pageable page = pagination(0, 10, "DESC", "createdAt");
        WorkSearchRequest input = new WorkSearchRequest();

        Mockito.when(workrepository.findAll(input.getWorkName(), input.getStartDate(), input.getEndDate(),
                input.getStatus(), page)).thenReturn(getListWork());

        Page<Work> list = workService.findAll(input, page);
        Assert.assertTrue(list.getNumberOfElements() == 2);
    }

    @Test
    public void testUpdateWorks() throws Exception {

        WorkEditRequest workEditRequest = new WorkEditRequest();

        Optional<Work> workFind = Optional.of(new Work());
        Mockito.when(workrepository.findById(Mockito.anyInt())).thenReturn(workFind);

        Work newWork = new Work();
        newWork.setUpdatedAt(new Date());
        Mockito.when(workrepository.save(Mockito.any(Work.class))).thenReturn(newWork);

        Work work = workService.update(1, workEditRequest);
        Assert.assertTrue(work.getUpdatedAt() != null);
    }

    private Page<Work> getListWork() {
        PageRequest paginacao = PageRequest.of(1, 10);
        List<Work> list = Arrays.asList(new Work(), new Work());
        Page<Work> rs = new PageImpl<>(list, paginacao, list.size());
        return rs;
    }
}
