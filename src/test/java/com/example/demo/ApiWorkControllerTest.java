package com.example.demo;

import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.demo.controllers.AbstractController;
import com.example.demo.entities.Work;
import com.example.demo.models.WorkEditRequest;
import com.example.demo.models.WorkRegisterRequest;
import com.example.demo.models.WorkSearchRequest;
import com.example.demo.services.WorkService;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class ApiWorkControllerTest extends AbstractController {
    @MockBean
    private WorkService workService;
    ObjectMapper mapper = new ObjectMapper();

    public static final MediaType APPLICATION_JSON_UTF8 = new MediaType(MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));

    @Before
    public void setup() {
        // test find all
        Mockito.when(workService.findAll(Mockito.any(WorkSearchRequest.class), Mockito.any(Pageable.class)))
                .thenReturn(getListWork());

        // test add
        Work w = new Work();
        w.setWorkName("test");
        Mockito.when(workService.save(Mockito.any(WorkRegisterRequest.class))).thenReturn(w);

        // test update
        Work w1 = new Work();
        w1.setId(1);
        w1.setUpdatedAt(new Date());
        Mockito.when(workService.update(Mockito.any(Integer.class), Mockito.any(WorkEditRequest.class))).thenReturn(w1);
    }

    private Page<Work> getListWork() {
        PageRequest paginacao = PageRequest.of(1, 10);
        List<Work> list = Arrays.asList(new Work(), new Work());
        Page<Work> rs = new PageImpl<>(list, paginacao, list.size());
        return rs;
    }

    @Test
    public void findWorksAll() throws Exception {
        Pageable page = pagination(0, 10, null, null);
        Page<Work> list = workService.findAll(new WorkSearchRequest(), page);
        Assert.assertTrue(list.getNumberOfElements() == 2);
    }

    @Test
    public void addWorks() throws Exception {
        Work rs = workService.save(new WorkRegisterRequest());
        Assert.assertTrue("test".equals(rs.getWorkName()));
    }

    @Test
    public void editWorks() throws Exception {
        Work rs = workService.update(1, new WorkEditRequest());
        Assert.assertTrue(1 == rs.getId());
        Assert.assertTrue(rs.getUpdatedAt() != null);
        Assert.assertTrue(rs.getDeletedAt() == null);
    }

}
