package com.example.demo.controllers;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;

public class AbstractController {
    protected Pageable pagination(Integer pageIndex, Integer pageSize, String typeSort, String columnSort) {
        pageIndex = (pageIndex == null || pageIndex == 0) ? 0 : pageIndex - 1;
        pageSize = (pageSize == null || pageSize == 0) ? 10 : pageSize;

        Direction sort = Direction.DESC;
        if (typeSort != null) {
            sort = typeSort.toUpperCase().equals("DESC") ? Direction.DESC : Direction.ASC;
        }

        return PageRequest.of(pageIndex, pageSize, sort, columnSort == null ? "createdAt" : columnSort);
    }
}
