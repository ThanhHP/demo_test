package com.example.demo.controllers;

import java.util.HashMap;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.common.ObjectCommonUtils;
import com.example.demo.common.UrlConstants;
import com.example.demo.entities.Work;
import com.example.demo.models.ResponseAPI;
import com.example.demo.models.WorkEditRequest;
import com.example.demo.models.WorkRegisterRequest;
import com.example.demo.models.WorkSearchRequest;
import com.example.demo.services.WorkService;
import com.example.demo.validator.WorkValidate;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@Api(value = "API.Works")
@RequestMapping(value = UrlConstants.API + UrlConstants.API_WORK)
public class ApiWorkController extends AbstractController{
    @Autowired
    WorkService workService;

    @Autowired
    WorkValidate workValidate;
    
    @ApiOperation(value = "Get list Works")
    @RequestMapping(value = UrlConstants.API_WORK_LIST, method = RequestMethod.POST)
    public ResponseAPI findAll(@RequestBody WorkSearchRequest input) throws Exception {
        // remove all field blank and empty
        ObjectCommonUtils.removeEmptyField(input);
        workValidate.validateDateFormTo(input.getStartDate(), input.getEndDate());
        // Setting paging
        Pageable page = pagination(input.getPageIndex(), input.getPageSize(), input.getTypeSort(), input.getColumnSort());
        Page<Work> rs = workService.findAll(input, page);

        HashMap<Object, Object> map = new HashMap<>();
        map.put("results", rs.getContent());
        map.put("totalRows", rs.getTotalElements());
        map.put("totalPages", rs.getTotalPages());
        map.put("currentPage", rs.getNumber() + 1);
        return new ResponseAPI(map, HttpStatus.OK);
    }

    @ApiOperation(value = "Add Work")
    @RequestMapping(value = UrlConstants.API_WORK_ADD, method = RequestMethod.POST)
    public ResponseAPI add(@RequestBody WorkRegisterRequest workRegisterRequest) throws Exception {
        workValidate.validateDateFormTo(workRegisterRequest.getStartDate(), workRegisterRequest.getEndDate());
        Work rs = workService.save(workRegisterRequest);
        return new ResponseAPI(rs, HttpStatus.OK);
    }

    @ApiOperation(value = "Edit Work")
    @RequestMapping(value = UrlConstants.API_WORK_EDIT, method = RequestMethod.POST)
    public ResponseAPI edit(@PathVariable(value = "id") int id, @RequestBody WorkEditRequest workEditRequest) throws Exception {
        workValidate.validateDateFormTo(workEditRequest.getStartDate(), workEditRequest.getEndDate());
        Work rs = workService.update(id, workEditRequest);
        if(rs == null) {
            return new ResponseAPI(null, HttpStatus.NOT_FOUND, "work not found!");
        }
        return new ResponseAPI(rs, HttpStatus.OK);
    }

    @ApiOperation(value = "Delete Work")
    @RequestMapping(value = UrlConstants.API_WORK_DELETE, method = RequestMethod.GET)
    public ResponseAPI delete(@PathVariable(value = "id") int id) {
        Optional<Work> work = workService.findById(id);
        if(Optional.empty().equals(work)) {
            return new ResponseAPI(null, HttpStatus.NOT_FOUND, "work not found!");
        }
        workService.delete(id);
        return new ResponseAPI(null, HttpStatus.OK);
    }
}
