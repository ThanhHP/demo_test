package com.example.demo.common;

public class UrlConstants {
    public static final String API              =   "/api";
    public static final String API_WORK         =   "/works";
    public static final String API_WORK_LIST    =   "/search";
    public static final String API_WORK_ADD     =   "/add";
    public static final String API_WORK_EDIT    =   "/update/{id}";
    public static final String API_WORK_DELETE  =   "/delete/{id}";;
}
