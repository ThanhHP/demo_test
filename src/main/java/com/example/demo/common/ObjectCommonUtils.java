package com.example.demo.common;

import java.lang.reflect.Field;

public class ObjectCommonUtils {

    public static void removeEmptyField(Object object) {
        Field[] fields = object.getClass().getDeclaredFields();
        setNullForEmptyFields(object, fields);
        if (object.getClass().getSuperclass() != null) {
            Field[] supperClassFields = object.getClass().getSuperclass().getDeclaredFields();
            setNullForEmptyFields(object, supperClassFields);
        }
    }

    private static void setNullForEmptyFields(Object object, Field[] fields) {
        for (Field field : fields) {
            try {
                field.setAccessible(true);
                if (field.get(object) !=null && "".equals(field.get(object).toString().trim())) {
                    field.set(object, null);
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
    }
}
