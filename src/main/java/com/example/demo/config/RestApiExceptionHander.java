package com.example.demo.config;

import static org.slf4j.LoggerFactory.getLogger;
import static org.springframework.http.HttpStatus.BAD_REQUEST;

import org.slf4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.example.demo.models.ResponseAPI;

@ControllerAdvice(annotations = RestController.class)
public class RestApiExceptionHander extends ResponseEntityExceptionHandler {

    private static final Logger log = getLogger(RestApiExceptionHander.class);

    @ExceptionHandler(value = { Exception.class, RuntimeException.class })
    @ResponseBody
    public ResponseEntity<ResponseAPI> handleGenericException(Exception ex, WebRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("handling exception...");
        }
        return new ResponseEntity<>(new ResponseAPI(null, HttpStatus.BAD_REQUEST, ex.getMessage()), BAD_REQUEST);
    }

}