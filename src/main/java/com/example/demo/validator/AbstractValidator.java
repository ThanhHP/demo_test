package com.example.demo.validator;

import java.util.List;

public abstract class AbstractValidator<INPUT, EXCEPTION> implements IValidator<INPUT, Exception> {
    @Override
    public void validate(INPUT input) throws Exception {
    };
    @Override
    public void validate(List<INPUT> inputList) throws Exception {
    };
}
