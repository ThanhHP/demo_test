package com.example.demo.validator;

import java.util.Date;

import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import com.example.demo.models.WorkRegisterRequest;

@Component
public class WorkValidate extends AbstractValidator<WorkRegisterRequest, Exception> {
    public void validateDateFormTo(Date from, Date to) throws Exception {
        if (from != null & to != null) {
            Assert.isTrue(!from.after(to), "Start date cannot be less than end date");
            ;
        }
    }
}