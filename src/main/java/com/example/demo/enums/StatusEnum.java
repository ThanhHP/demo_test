package com.example.demo.enums;

public enum StatusEnum {
    PLANNING,
    DOING, 
    COMPLETE
}
