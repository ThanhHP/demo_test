package com.example.demo.models;

import java.io.Serializable;

import org.springframework.http.HttpStatus;

public class ResponseAPI implements Serializable {

    private static final long serialVersionUID = 1L;

    private HttpStatus code;
    private String message;
    private Object data;

    public ResponseAPI() {
    }

    public ResponseAPI(Object data, HttpStatus code, String message) {
        this.data = data;
        this.code = code;
        this.message = message;
    }

    public ResponseAPI(Object data, HttpStatus code) {
        this.data = data;
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setCode(HttpStatus code) {
        this.code = code;
    }

    public HttpStatus getCode() {
        return code;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
