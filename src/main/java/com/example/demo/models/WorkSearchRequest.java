package com.example.demo.models;

import java.util.Date;

import com.example.demo.enums.StatusEnum;
import com.fasterxml.jackson.annotation.JsonFormat;

public class WorkSearchRequest {
    private Integer pageIndex;
    private Integer pageSize;
    private String columnSort;
    private String typeSort;

    private String workName;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private Date startDate;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private Date endDate;
    private StatusEnum status;

    public Integer getPageIndex() {
        return pageIndex;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public String getColumnSort() {
        return columnSort;
    }

    public String getTypeSort() {
        return typeSort;
    }

    public String getWorkName() {
        return workName;
    }

    public Date getStartDate() {
        return startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public StatusEnum getStatus() {
        return status;
    }

    public void setPageIndex(Integer pageIndex) {
        this.pageIndex = pageIndex;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public void setColumnSort(String columnSort) {
        this.columnSort = columnSort;
    }

    public void setTypeSort(String typeSort) {
        this.typeSort = typeSort;
    }

    public void setWorkName(String workName) {
        this.workName = workName;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public void setStatus(StatusEnum status) {
        this.status = status;
    }
}
