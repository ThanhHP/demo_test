package com.example.demo.models;

import java.util.Date;

import com.example.demo.enums.StatusEnum;
import com.fasterxml.jackson.annotation.JsonFormat;

public class WorkRegisterRequest {
    private String workName;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private Date startDate;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private Date endDate;
    private StatusEnum status;

    public String getWorkName() {
        return workName;
    }

    public Date getStartDate() {
        return startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public StatusEnum getStatus() {
        return status;
    }

    public void setWorkName(String workName) {
        this.workName = workName;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public void setStatus(StatusEnum status) {
        this.status = status;
    }
}
