package com.example.demo.repositories;

import java.util.Date;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.demo.entities.Work;
import com.example.demo.enums.StatusEnum;

@Repository
public interface WorkRepository extends JpaRepository<Work, Integer> {

    @Query(value="SELECT w "
            + "     FROM Work w "
            + "     WHERE w.deletedAt is null"
            + "         AND (:workName is null or w.workName = :workName )"
            + "         AND (:startDate is null or DATE(w.startDate) = DATE(:startDate) )"
            + "         AND (:endDate is null or DATE(w.endDate) = DATE(:endDate) )"
            + "         AND (:status is null or w.status = :status )"
            )
    Page<Work> findAll(@Param(value="workName") String workName
            , @Param(value="startDate") Date startDate
            , @Param(value="endDate") Date endDate
            , @Param(value="status") StatusEnum status
            , Pageable page);

}
