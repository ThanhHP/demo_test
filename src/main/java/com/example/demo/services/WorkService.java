package com.example.demo.services;


import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.example.demo.entities.Work;
import com.example.demo.models.WorkEditRequest;
import com.example.demo.models.WorkRegisterRequest;
import com.example.demo.models.WorkSearchRequest;

public interface WorkService {
    Page<Work> findAll(WorkSearchRequest input, Pageable page);

    Work save(WorkRegisterRequest workRegisterRequest);

    Work update(int id, WorkEditRequest workEditRequest);

    void delete(int id);

    Optional<Work> findById(int id);
}
