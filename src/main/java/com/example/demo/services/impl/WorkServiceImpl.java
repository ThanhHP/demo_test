package com.example.demo.services.impl;

import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.example.demo.entities.Work;
import com.example.demo.models.WorkEditRequest;
import com.example.demo.models.WorkRegisterRequest;
import com.example.demo.models.WorkSearchRequest;
import com.example.demo.repositories.WorkRepository;
import com.example.demo.services.WorkService;

@Service
@Component("workService")
public class WorkServiceImpl implements WorkService {

    @Autowired
    WorkRepository workrepository;
    @Override
    public Page<Work> findAll(WorkSearchRequest input, Pageable page) {
        return workrepository.findAll(input.getWorkName()
                , input.getStartDate()
                , input.getEndDate()
                , input.getStatus()
                , page);
    }
    @Override
    public Work save(WorkRegisterRequest workRegisterRequest) {
        Work work = new Work();
        BeanUtils.copyProperties(workRegisterRequest, work);
        return workrepository.save(work);
    }
    @Override
    public Work update(int id, WorkEditRequest workEditRequest) {
        Optional<Work> work = workrepository.findById(id);
        if(!Optional.empty().equals(work)) {
            Work oj = work.get();
            BeanUtils.copyProperties(workEditRequest, oj);
            return workrepository.save(oj);
        }
        return null;
    }
    @Override
    public void delete(int id) {
        workrepository.deleteById(id);
    }
    @Override
    public Optional<Work> findById(int id) {
        return workrepository.findById(id);
    }
    
}
